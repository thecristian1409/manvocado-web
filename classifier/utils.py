from __future__ import print_function
import numpy as np
from keras.models import model_from_json
import tensorflow as tf

'''
Image classification with a pretrained CNN, reusing ResNet50 with the given parameters

    Returns a numpy array of predictions
'''
def resNetPredictor(model, image_path):

    with graph.as_default():
        # image = getImageData(image_path,0,0)
        image = np.expand_dims(image_path, axis=0)
        # image = np.transpose(image, (0, 3, 2, 1)) 
        score = model.predict(image)

    return score

def loadModel():

    global graph
    graph = tf.get_default_graph()

    json_file = open('classifier/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    return loaded_model

def loadWeights(model):

    with graph.as_default():
        model.load_weights('classifier/model_weights.h5')

    return model