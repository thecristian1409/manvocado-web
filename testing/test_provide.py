from io import BytesIO
from app.routes import app # IMPORT YOU FLASK APP HERE
import app.aws_bucket as bucket
import pytest
import base64

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client


def test_file_upload(client):

    with open("testing/images/test.png", "rb") as image:
        f = image.read()
        f = base64.b64encode(f)


    data = {
        'file': f,
        'classification': 'test',
        'n': '0'
    }
    # note in that in the previous line you can use 'file' or whatever you want.
    # flask client checks for the tuple (<FileObject>, <String>)
    res = client.post('/provide', data=data) 
    print(res.get_json())
    assert res.status_code == 200

    res = bucket.remove_file()
    assert res.status_code == 200

def test_file_upload_wrong_dim(client):

    with open("testing/images/test_wrong_dim.png", "rb") as image:
        f = image.read()
        f = base64.b64encode(f)

    data = {
        'file': f,
        'classification': 'test'
    }

    res = client.post('/provide', data=data)
    assert res.status_code == 400