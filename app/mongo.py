import pymongo
import json

class Database:

    def __init__(self):

        URI, database, collection = self.getConnection()
        client = pymongo.MongoClient(URI)
        db = client[database]

        self.collection = collection
        self.db = db 

        print('Welcome to database ' + str(database))

    def getConnection(self):
        with open ('config.json') as file:
            data = json.load(file)
            URI = data['mongoDBConnection']
            database = data['mongoDBDatabase']
            collection = data['mongoDBCollection']
            
            return URI, database, collection

    def insert(self, value):
        self.db[self.collection].insert_one(value)

    def remove(self, url):
        self.db[self.collection].delete_one({ 'URL': url })

    def update_votes(self, images_URL, classification):
        '''
        Images_URL has a list of json objects, each one containing the field URL.
        Classification is the class that receives the votes
        '''

        if classification == "Aguacate":
            classification = "votesAvocado"
        elif classification == "Mango":
            classification = "votesMango"
        else:
            classification = "votesBuilding"
        

        for image in images_URL:

            self.db[self.collection].update_one(
                {
                    'URL': image['value']
                }, {
                    '$inc': {
                        classification: 1
                    }
                }
            )


    def sampleRandomImages(self, n=4, images_in_use=[]):

        if images_in_use == []:
            return list(self.db[self.collection].aggregate(
                [
                {
                    '$match': {
                        'class': {
                            '$in': ['pending']
                        }
                    }
                }, {
                    '$project': {
                        '_id': 0, 
                        'class': 0,
                        'votesAvocado': 0,
                        'votesMango': 0,
                        'votesBuilding': 0
                    }
                }, {
                    '$sample': {
                        'size': n
                    }
                }
                ]
            ))
        else: 

            return list(self.db[self.collection].aggregate([
                {
                    '$match': {
                        'URL': {
                            '$nin' : [img['URL'] for img in images_in_use]
                        },
                        'class': {
                            '$in' : ['pending'] 
                        }
                    }
                }, {
                    '$project': {
                        '_id': 0, 
                        'class': 0,
                        'votesAvocado': 0,
                        'votesMango': 0,
                        'votesBuilding': 0
                    }
                }, {
                    '$sample': {
                        'size': n
                    }
                }
            ]))



'''
TRIGGER CODE:

exports = function(changeEvent) {
  /*
    A Database Trigger will always call a function with a changeEvent.
    Documentation on ChangeEvents: https://docs.mongodb.com/manual/reference/change-events/
  */
  const fullDocument = changeEvent.fullDocument;

  const totalVotes = fullDocument.votesAvocado + fullDocument.votesMango + fullDocument.votesBuilding;
  
  if (fullDocument.class == "pending"){
    
    const mongodb = context.services.get("Cluster0");
    const manvo = mongodb.db("Images").collection("Manvocado");
    

    if (totalVotes >= 10) {
      if (fullDocument.votesAvocado/totalVotes >= 0.75) {
        return manvo.updateOne(
          { "_id": fullDocument._id  },
          { "$set": { "class" : "avocado" } }  
        );
      } else if (fullDocument.votesMango/totalVotes >= 0.75) {
        return manvo.updateOne(
          { "_id": fullDocument._id },
          { "$set": { "class" : "mango" } }  
        );        
      } else if (fullDocument.votesBuilding/totalVotes >= 0.75) {
        return manvo.updateOne(
          { "_id": fullDocument._id  },
          { "$set": { "class" : "building" } }    
        );        
      } else {
      }
    }
  }
  
};

TRIGGER TEST CODE:


/*
  To Run the function:
    - Type 'exports(Argument);' to run the function with an object argument
    - Click 'Run'

  A Database Trigger will always be called with a changeEvent describing the change that happened in the database.
  Learn more in our ChangeEvent documentation: https://docs.mongodb.com/manual/reference/change-events/
*/
const changeEvent = {
   _id : "a",
   "operationType": "a",
   "fullDocument": {
     "_id" : "5e83021b021c0912d1bf1732",
     "URL" : "https://manvocado.s3.eu-west-3.amazonaws.com/image5.png",
     "votesAvocado" : 10,
     "votesMango" : 0,
     "votesBuilding" : 0,
     "class" : "pending"
   },
   "ns": {
      "db" : "Images",
      "coll" : "Manvocado"
   },
   "documentKey": {
     "_id": "a"
   },
   "updateDescription": {
      "a": "b"
   },
   "clusterTime": {
     "a": "b"
   }
} 

exports(changeEvent);

'''