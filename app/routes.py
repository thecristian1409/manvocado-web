import os
from flask import Flask, request, Response, render_template
from pymongo import MongoClient
from PIL import Image
import numpy as np
import io
import cv2
import json
import base64
import classifier.utils as u
import app.aws_bucket as bucket
from app.mongo import Database


app = Flask(__name__)

app.secret_key = "super secret key"

areWeightsLoaded = False

model = u.loadModel()

client = Database()

images_in_use = client.sampleRandomImages()


@app.route('/', methods=['GET'])
def index():

    global images_in_use 
    images_in_use = client.sampleRandomImages()

    return render_template('index.html', image1=images_in_use[0]['URL'], image2=images_in_use[1]['URL'], image3=images_in_use[2]['URL'], image4=images_in_use[3]['URL'])
    # return render_template('index.html')

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':

        file = request.form['file']

        fixedImage = file.replace(' ', '+')
        image = base64.b64decode(fixedImage)

        image = np.array(Image.open(io.BytesIO(image)))
        image = cv2.cvtColor(image, cv2.COLOR_BGRA2BGR)
        print(image.shape)

        if image.shape[0] != 256 or image.shape[1] != 256 or image.shape[2] != 3:
            response = {'404': 'Las dimensiones de la imagen no son correctas'}
            return Response(json.dumps(response), mimetype='application/json', status=400)

        global model
        global areWeightsLoaded

        if not areWeightsLoaded:
            model = u.loadWeights(model)
            areWeightsLoaded = True

        data = u.resNetPredictor(model,image)
        
        json_data = {'Avocado': float(data[0,0]),
                'Mango': float(data[0,1]),
                'Building': float(data[0,2])
               }
        
        return Response(json.dumps(json_data), mimetype='application/json', status=200)


@app.route('/provide', methods=['GET', 'POST'])
def provide():
    if request.method == 'POST':
        image = request.form['file']
        classification = request.form['classification']
        n = request.form['n']

        #Check for the image's dimensions

        fixedImage = image.replace(' ', '+')
        fixedImage = base64.b64decode(fixedImage)

        fixedImage = np.array(Image.open(io.BytesIO(fixedImage)))
        fixedImage = cv2.cvtColor(fixedImage, cv2.COLOR_BGRA2BGR)
        print(fixedImage.shape)

        if fixedImage.shape[0] != 256 or fixedImage.shape[1] != 256 or fixedImage.shape[2] != 3:
            response = {'404': 'Las dimensiones de la imagen no son correctas'}
            return Response(json.dumps(response), mimetype='application/json', status=400)


        link = bucket.upload_file(image, int(n))
        print(link)

        upload_to_mongo(classification, link)

        return Response(json.dumps({'Status': 200}), mimetype='application/json', status=200)


@app.route('/classify', methods=['POST'])
def classify():
    if request.method == 'POST':

        #Classify
        if request.form['images'] != "reload":
            selected_images = request.form['images']
            classification = request.form['classification']
            selected_images = json.loads(selected_images)


            new_images = []

            image_replacements = client.sampleRandomImages(n=len(selected_images), images_in_use=images_in_use)

            #For each selected image
            for images in selected_images:

                #Replacing the selected images for new ones
                #Structure of the json: {"key":"img1","value":"https://..."}
                replacement = image_replacements.pop()['URL']
                change_images_in_use(replacement, images)

                new_images.append({"key": images['key'], "value": replacement})

            #Update votes in mongo
            client.update_votes(selected_images, classification)


            return Response(json.dumps(new_images), mimetype='application/json', status=200)
        
        #Reload images
        else:

            new_images = []

            image_replacements = client.sampleRandomImages(n=len(images_in_use), images_in_use=images_in_use)
            n_img = 1

            #For each selected image
            for _ in images_in_use:
                #Replacing the selected images for new ones
                #Structure of the json: {"key":"img1","value":"https://..."}
                #Thus we need to parse it

                replacement = image_replacements.pop()['URL']
                change_images_in_use(replacement, "", "img" + str(n_img))

                new_images.append({"key": "img" + str(n_img), "value": replacement})

                n_img = n_img + 1
                
            return Response(json.dumps(new_images), mimetype='application/json', status=200)

@app.route('/thanks')
def thanks():
    return render_template('thanks.html')




'''
--------------
Useful methods
--------------
'''

def change_images_in_use(replacement, images, n_img=""):

    global images_in_use

    if n_img != "":
        index = int(n_img.replace("img", ""))
    else:
        index = int(images['key'].replace("img", ""))

    images_in_use[index-1]['URL'] = replacement

def upload_to_mongo(classification, url):
    if classification == "avocado":
        row = {"class": "pending", "URL": url, "votesAvocado": 1, "votesMango": 0, "votesBuilding": 0}
    elif classification == "mango":
        row = {"class": "pending", "URL": url, "votesAvocado": 0, "votesMango": 1, "votesBuilding": 0}
    else:
        row = {"class": "pending", "URL": url, "votesAvocado": 0, "votesMango": 0, "votesBuilding": 1}

    client.insert(row)
