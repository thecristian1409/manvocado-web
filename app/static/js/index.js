var file = null;
var hasImage = false;
var hasClassification = false;
var properDimensions = true;



function handleFilesProv(files) {

    //We reset fields if there were previously loaded images
    document.getElementById('canvasProv').innerHTML = "";
    properDimensions = true;
    document.getElementById('provError').innerHTML = "";


    files = files.target.files;
    files = [...files]
    files.forEach(previewFile)

    // this is to read the file
    hasImage = true;

    if (hasImage){
        document.getElementById('selected_image').style.display = 'block';
    }

}

function previewFile(file){
    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = function() {
        let img = document.createElement('img');
        img.src = reader.result;
        document.getElementById('canvasProv').appendChild(img);

        var image = new Image();       
        image.onload = function(){
            if (image.width != 256 || image.height != 256){
                properDimensions = false;
                document.getElementById('provError').display = 'block';
                document.getElementById('provError').innerHTML = '<p>Las dimensiones de la imagen no son correctas, por favor suba una imagen/imágenes RGB de 256x256 píxeles</p>';
            } 
        };
        image.src = img.src;

    }
}

function handleFilesPred(e) {
    var ctx = document.getElementById('canvasPred').getContext('2d');
    var reader  = new FileReader();
    file = e.target.files[0];
    // load the image to get its width/height
    var img = new Image();
    img.onload = function() {
        // scale canvas to image
        ctx.canvas.width = img.width;
        ctx.canvas.height = img.height;
        // draw image
        ctx.drawImage(img, 0, 0, ctx.canvas.width, ctx.canvas.height );
    }
    // this is to load the image
    reader.onloadend = function () {
        img.src = reader.result;
    }

    document.getElementById('upload_button_pred').style.display = 'block';

    reader.readAsDataURL(file);
}

function classified(){
    hasClassification = true;
    if (hasImage && hasClassification && properDimensions) {
        document.getElementById('upload_button_prov').style.display = 'block';
    } 
}

function changeTab(evt, b) {

    var i, tablinks;

    tabs = document.getElementsByClassName("tab-content");
    tablinks = document.getElementsByClassName("tablinks");

    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        tabs[i].style.display = "none";
    }

    document.getElementById(b).style.display = "block";
    evt.currentTarget.className += " active";


    //In case we selected images in the classification tab, we deselect them

    grid = document.getElementsByClassName("grid_img_selected");
    for (i = 0; i < grid.length; i++) {
        grid[i].className = grid[i].className.replace("_selected", "");
    }   

  }

function httpAsync(url, type, parameter, callback, errorCallback) {
    /*
     * Async HTTP get request
     * https://stackoverflow.com/a/4033310
     */
    

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.response);
        else if(xmlHttp.readyState == 4)
            errorCallback();

    }
    xmlHttp.open(type, url, true); // true for asynchronous 
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send(parameter);
    
}

async function predictImage() {

    var parameter = ["file=" + prepareImage()];

    httpAsync("/predict", "POST", parameter,
        function(response) {   
            document.getElementById('textPredValues').style.display = 'block';
            document.getElementById('predValues').display = 'block';

            jsonResponse = JSON.parse(response);

            //If the prediction is not accurate at all, we say the model wasn't able to determine a class
            if (jsonResponse.Avocado < 0.6 && jsonResponse.Mango < 0.6 && jsonResponse.Building < 0.6){                
                document.getElementById('textPredValues').innerHTML = "No se ha podido determinar la clase de la imagen.";
                document.getElementById('predValues').innerHTML = '';

            } else {

                jsonResponse = JSON.parse(response)

                if (jsonResponse.Avocado > jsonResponse.Mango && jsonResponse.Avocado > jsonResponse.Building) {
                    htmlCode = '<ul id="predictions">' +
                        '<li class="aguacate">Aguacate</il>' +
                    '</ul>'
                    document.getElementById('textPredValues').innerHTML = "La imagen pertenece a la siguiente clase con una precisión del " + parseFloat(jsonResponse.Avocado).toFixed(4)*100 + "%";

                } else if (jsonResponse.Mango > jsonResponse.Avocado && jsonResponse.Mango > jsonResponse.Building) {
                    htmlCode = '<ul id="predictions">' +
                        '<li class="mango">Mango</il>' +
                    '</ul>'
                    document.getElementById('textPredValues').innerHTML = "La imagen pertenece a la siguiente clase con una precisión del " + parseFloat(jsonResponse.Mango).toFixed(4)*100 + "%";

                } else {

                    htmlCode = '<ul id="predictions">' +
                        '<li class="construccion">Construcción</il>' +
                    '</ul>'
                    document.getElementById('textPredValues').innerHTML = "La imagen pertenece a la siguiente clase con una precisión del " + parseFloat(jsonResponse.Building).toFixed(4)*100 + "%";

                }
                document.getElementById('predValues').innerHTML = htmlCode;
            }
        },
        function() {
            document.getElementById('predValues').display = 'block';
            document.getElementById('predValues').innerHTML = '<p>Las dimensiones de la imagen no son correctas, por favor suba una imagen RGB de 256x256 píxeles</p>';
        }
    );

}

async function provideImage(){

    var children = document.getElementById('canvasProv').children;

    //We retrieve the class that the user assigned
    var ele = document.getElementsByName('button'); 
    var classification;   
    for(i = 0; i < ele.length; i++) { 
        if(ele[i].checked) 
            classification = ele[i].value;
    } 


    for (var i = 0; i < children.length; i++) {

        var encoded = children[i].src.split(',')[1];
        var parameter = "file=" + encoded + '&classification=' + classification + '&n=' + i;
    
        httpAsync("/provide", "POST", parameter,
            function(response) {   
                document.location.href = '/thanks';
            },
            function() {
            }
        );
        
    }
    
}

function prepareImage() {

    if(document.getElementById('image_pred').value == '')
        return null;
    var canvas = document.getElementById('canvasPred');
        
    var dataURI = canvas.toDataURL();

    var encoded = dataURI.split(',')[1];
    
    return encoded;
}


async function selectImage(evt){

    if (evt.currentTarget.className.includes("_selected")){
        evt.currentTarget.className = evt.currentTarget.className.replace("_selected", "");
    } else {
        evt.currentTarget.className += "_selected";
    }
    
}

async function classify(){
  
    var selectedImages = [].slice.apply(document.getElementsByClassName("grid_img_selected")); //BECAUSE JS ENJOYS DYNAMIC REFERENCES :)
    var classified = [];
    console.log(selectedImages);

    for (i in selectedImages) {

        //We send both the image from the grid and the image URL to know which was the one voted.
        classified.push({
            key: selectedImages[i].id,
            value: document.getElementById(selectedImages[i].id).src
        });
        selectedImages[i].className = selectedImages[i].className.replace("_selected", "");
    }    

    var parameter = "images=" + JSON.stringify(classified) + '&classification=' + document.getElementById("class_to_classify").innerHTML.split(": ")[1];


    httpAsync("/classify", "POST", parameter,
        function(response) {   

            jsonResponse = JSON.parse(response);

            for (img in jsonResponse) {
                document.getElementById(jsonResponse[img].key).src = jsonResponse[img].value;
            }

            var classification = document.getElementById("class_to_classify").innerHTML.split(": ");
            if (classification[1] == "Aguacate") {
                document.getElementById("class_to_classify").innerHTML = classification[0] + ": Mango"; 
            } else if (classification[1] == "Mango") {
                document.getElementById("class_to_classify").innerHTML = classification[0] + ": Construcción"; 
            } else {
                document.getElementById("class_to_classify").innerHTML = classification[0] + ": Aguacate"; 
            }

        },
        function() {
            console.log("IT DOESN'T WORK");
        }
    );
}

async function reload(){
  
    httpAsync("/classify", "POST", "images=reload",
        function(response) {   

            jsonResponse = JSON.parse(response);

            for (img in jsonResponse) {
                document.getElementById(jsonResponse[img].key).src = jsonResponse[img].value;
            }

        },
        function() {
            console.log("IT DOESN'T WORK");
        }
    );
}
