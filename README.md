# Manvocado-Web

Aplicación web desarrollada para el Trabajo de Fin de Grado, pensada para integrar el modelo de Redes Neuronales Convolucionales propuesto para clasificar árboles de cultivos tropicales y ofrecer un servicio de etiquetado de imágenes para el enriquecimiento del dataset.

Para recrear el entorno de desarrollo, se requiere de Python 3.6.9 y de las librerías que se encuentran en requirements.txt.

`pip install -r requirements.txt`

El archivo config.json debe contener las credenciales que utiliza la aplicación para acceder a la base de datos (MongoDB) y a Amazon Web Services, se deben especificar para poder lanzar la aplicación.

Para lanzar la aplicación:

`flask run`
